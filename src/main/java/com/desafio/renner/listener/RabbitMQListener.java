package com.desafio.renner.listener;

import com.desafio.renner.dto.VendaRequest;
import com.desafio.renner.service.AutorizarVendaService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class RabbitMQListener {
    @Autowired
    private AutorizarVendaService service;

    @RabbitListener(queues = "autorizar-venda-queue")
    public void processMessage(String message) throws JsonProcessingException {
        log.info("Received Message: " + message);
        try {
            ObjectMapper objectMapper =
                    new ObjectMapper().registerModule(new JavaTimeModule())
                            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
            VendaRequest vendaRequest = objectMapper
                    .readValue(message, VendaRequest.class);


            service.processarAutorizacao(vendaRequest);

        } catch (JsonParseException | JsonMappingException e) {
            log.error("Invalid json payload {}", e.getMessage());
        } catch (Exception e) {
            log.error(" error - {}", e.getMessage());
            throw e;
        }
    }
}
