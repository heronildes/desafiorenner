package com.desafio.renner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DesafioRennerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioRennerApplication.class, args);
	}

}
