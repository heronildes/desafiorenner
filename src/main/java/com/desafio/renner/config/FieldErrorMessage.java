package com.desafio.renner.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FieldErrorMessage {

	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	private String field;
	private String message;

	public FieldErrorMessage(String message) {
		this.message = message;
	}
	
}
