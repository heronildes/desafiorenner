package com.desafio.renner.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.UnexpectedTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private static final String VALIDATION = "Validation";

    @Autowired
    private MessageSource messageSource;


    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({IllegalArgumentException.class, JsonMappingException.class})
    public ErrorMessage mappingError(Exception exception) {
        return buildErrorMessage(ErrorCodes.BAD_REQUEST, listFieldErros(exception));
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ErrorMessage mappingError(MissingServletRequestParameterException ex) {
        return buildErrorMessage(ErrorCodes.BAD_REQUEST,
                List.of(new FieldErrorMessage(ex.getParameterName(),
                        ErrorCodes.REQUIRED_PARAMETER.getMessage())));
    }


    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public final ErrorMessage handlerHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex) {
        return buildErrorMessage(ErrorCodes.BAD_REQUEST, listFieldErros(ex));
    }


    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingRequestHeaderException.class)
    public ErrorMessage missingRequestHeaderHandler(MissingRequestHeaderException ex) {
        return ErrorMessage.builder().code(ErrorCodes.BAD_REQUEST.getCode()).message(ex.getLocalizedMessage()).build();
    }


    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingPathVariableException.class)
    public ErrorMessage handle(MissingPathVariableException exception) {
        return buildErrorMessage(ErrorCodes.BAD_REQUEST, listFieldErros(exception));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ClassNotFoundException.class)
    public ErrorMessage handleClassNotFoundException(ClassNotFoundException exception) {
        return buildErrorMessage(ErrorCodes.NOT_FOUND, listFieldErros(exception));
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public ErrorMessage handleNotFoundException(ChangeSetPersister.NotFoundException exception) {
        return buildErrorMessage(ErrorCodes.NOT_FOUND, listFieldErros(exception));
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ConstraintViolationException.class})
    public ErrorConstrainMessage handleValidationException(ConstraintViolationException exception) {

        List<FieldConstrainErrorMessage> fieldConstrainErrorMessageList = new ArrayList<>();

        exception.getConstraintViolations().forEach(ex -> {

            fieldConstrainErrorMessageList.add(listFieldConstrainErros(ex));
        });
        var list = fieldConstrainErrorMessageList.stream().distinct().collect(Collectors.toList());
        return buildConstrainErrorMessage(ErrorCodes.BAD_REQUEST, list);
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ErrorMessage handleNoHandlerFoundException(NoHandlerFoundException exception) {
        return buildErrorMessage(ErrorCodes.NOT_FOUND, listFieldErros(exception));
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorMessage handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {

        ErrorMessage response = new ErrorMessage();
        List<FieldErrorMessage> responseList = new ArrayList<>();
        Field[] clsFields = exception.getParameter().getParameter().getType().getDeclaredFields();
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        if (!fieldErrors.isEmpty()) {
            fieldErrors.forEach(e -> {
                String field = getJsonProperty(clsFields, e);
                FieldErrorMessage error = new FieldErrorMessage(field,
                        messageSource.getMessage(e, LocaleContextHolder.getLocale()));
                responseList.add(error);
            });

            response.setMessage(ErrorCodes.BAD_REQUEST.getMessage());
            response.setCode(ErrorCodes.BAD_REQUEST.getCode());
            response.setDetail(responseList);
            return response;
        }

        return response;
    }

    private String getJsonProperty(Field[] clsFields, FieldError fieldError) {
        JsonProperty[] alisas = getAnnotationsForField(clsFields, fieldError.getField());
        if (alisas == null || alisas.length == 0) {
            return fieldError.getField();
        }

        String values = alisas[0].value();
        if (values.length() == 0) {
            return fieldError.getField();
        }

        return values;
    }

    private JsonProperty[] getAnnotationsForField(Field[] clsFields, String fieldName) {
        Optional<Field> first = Stream.of(clsFields).filter(f -> f.getName().equals(fieldName)).findFirst();
        return first.map(field -> field.getAnnotationsByType(JsonProperty.class)).orElse(null);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ErrorMessage handleBindException(BindException exception) {

        ErrorMessage response = new ErrorMessage();
        List<FieldErrorMessage> responseList = new ArrayList<>();

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        if (!fieldErrors.isEmpty()) {
            fieldErrors.forEach(e -> {
                FieldErrorMessage error = new FieldErrorMessage(e.getField(),
                        messageSource.getMessage(e, LocaleContextHolder.getLocale()));
                responseList.add(error);
            });

            response.setMessage(ErrorCodes.BAD_REQUEST.getMessage());
            response.setCode(ErrorCodes.BAD_REQUEST.getCode());
            response.setDetail(responseList);
            return response;
        }

        return response;
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorMessage httpMessageNotReadableException(HttpMessageNotReadableException exception) {
        return ErrorMessage.builder().code(ErrorCodes.BAD_REQUEST.getCode())
                .message(ErrorCodes.BAD_REQUEST.getMessage()).build();
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UnexpectedTypeException.class)
    public ErrorMessage unexpectedTypeException(UnexpectedTypeException exception) {
        return ErrorMessage.builder().code(ErrorCodes.BAD_REQUEST.getCode())
                .message(ErrorCodes.BAD_REQUEST.getMessage()).build();
    }



    private static ErrorConstrainMessage buildConstrainErrorMessage(ErrorCodes error,
                                                                    List<FieldConstrainErrorMessage> responseList){

        return new ErrorConstrainMessage(error.getCode(), error.getMessage(), responseList);
    }

    private static ErrorMessage buildErrorMessage(ErrorCodes error, List<FieldErrorMessage> responseList) {
        return new ErrorMessage(error.getCode(), error.getMessage(), responseList);
    }

    private static List<FieldErrorMessage> listFieldErros(Exception ex) {
        return List.of(new FieldErrorMessage(null, ex.getLocalizedMessage()));
    }

    private static FieldConstrainErrorMessage listFieldConstrainErros(ConstraintViolation<?> ex) {

        String field = ex.getPropertyPath().toString();

        if(field.contains("execute.")){
            return new FieldConstrainErrorMessage(null, field.substring(field.lastIndexOf("execute.") + 8),
                    ex.getMessage());
        }
        return new FieldConstrainErrorMessage(null, field, ex.getMessage());
    }

}
