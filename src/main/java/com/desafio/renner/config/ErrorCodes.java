package com.desafio.renner.config;

import lombok.Getter;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

public enum ErrorCodes {

	BAD_REQUEST("BAD_REQUEST", "desafio.error.bad.request"),
	NOT_FOUND("NOT_FOUND", "desafio.error.not.found"),
	NOT_NULL("BAD_REQUEST", "desafio.error.field.null"),
	IS_EMPTY("BAD_REQUEST", "desafio.error.field.empty}"),
	REQUIRED_PARAMETER("REQUIRED_PARAMETER", "desafio.error.required.parameter");

	private final String message;
	@Getter
	private final String code;

	ErrorCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String getMessage(Object... values) {
		return getMessageSource().getMessage(this.message, values, LocaleContextHolder.getLocale());
	}

	private static ResourceBundleMessageSource getMessageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename("messages");
		return source;
	}
}
