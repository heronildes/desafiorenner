package com.desafio.renner.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class FieldConstrainErrorMessage {

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String index;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String field;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String message;

    public FieldConstrainErrorMessage(String message) {
        this.message = message;
    }
}
