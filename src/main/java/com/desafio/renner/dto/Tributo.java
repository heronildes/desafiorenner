package com.desafio.renner.dto;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Tributo {

     private Long sku;
     private Long valorIcms;
     private Long valorPis;
     private Long valorDifaul;
     private Long valorFcpIcms;
}
