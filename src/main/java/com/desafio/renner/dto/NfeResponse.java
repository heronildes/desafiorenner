package com.desafio.renner.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class NfeResponse {

    private String nfeKey;
    private Long invoiceNumber;
    private LocalDateTime issuanceDate;
    private String invoice;
}
