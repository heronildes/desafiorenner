package com.desafio.renner.dto;

import com.desafio.renner.enums.Estado;
import com.desafio.renner.enums.TipoDocumento;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Cliente {

    private Long id;
    private String nome;
    private String documento;
    private TipoDocumento tipoDocumento;
    private String tipoPessoa;
    private String endereco;
    private Integer numeroEndereco;
    private String complementoEndereco;
    private String bairro;
    private String cidade;
    private Estado estado;
    private String pais;
    private String cep;
    private Integer codigoIbge;
    private String telefone;
    private String email;
}
