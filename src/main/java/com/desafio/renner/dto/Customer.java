package com.desafio.renner.dto;

import com.desafio.renner.enums.Estado;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Customer {

    private Long id;
    private String name;
    private String document;
    private String documentType;
    private String personType;
    private String address;
    private Integer addressNumber;
    private String addressComplement;
    private String district;
    private String city;
    private Estado state;
    private String country;
    private String zipCode;
    private Integer ibgeCode;
    private String phoneNumber;
    private String email;
}
