package com.desafio.renner.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class CanalRequest {
    private Long numeroPedido;
    private String numeroOrdemExterno;
    private String chaveNFE;
    private Long numeroNota;
    private LocalDateTime dataEmissao;
    private String pdf;
    private String status;
}
