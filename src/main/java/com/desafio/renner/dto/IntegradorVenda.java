package com.desafio.renner.dto;

import com.desafio.renner.enums.Canal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class IntegradorVenda {

    private Long id;
    private Canal canal;
    private String empresa;
    private String loja;
    private Long pdv;
}
