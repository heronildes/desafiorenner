package com.desafio.renner.dto;

import com.desafio.renner.enums.Canal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.executable.ValidateOnExecution;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class VendaRequest {

    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.field.empty}")
    private String canal;
    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.field.empty}")
    private String empresa;
    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.field.empty}")
    private String loja;
    private Long pdv;
    @Valid
    private Ordem ordemPedido;
    @NotNull(message = "{desafio.error.field.null}")
    private Cliente cliente;
    @NotNull(message = "{desafio.error.field.null}")
    private Long totalItens;
    @NotNull(message = "{desafio.error.field.null}")
    private Long quantidadeItens;
    @Valid
    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.list.empty}")
    private List<@Valid Sku> itens;
    private LocalDateTime dataRequisicao;

}
