package com.desafio.renner.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Product {

    private Long sku;
    private Long amount;
    private Double value;
    private Long icmsValue;
    private Long pisValue;
    private Long difaulValue;
    private Long fcpIcmsValue;
}
