package com.desafio.renner.dto;

import com.desafio.renner.enums.VendaStatus;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@Getter
public class VendaResponse {

    private VendaStatus status;
    private LocalDateTime dataResposta;

}
