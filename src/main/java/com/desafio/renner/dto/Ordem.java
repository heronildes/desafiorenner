package com.desafio.renner.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class Ordem {
    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.field.empty}")
    private Long numeroPedido;
    @NotNull(message = "{desafio.error.field.null}")
    @NotEmpty(message = "{desafio.error.field.empty}")
    private String numeroOrdemExterno;
    @NotNull(message = "{desafio.error.field.null}")
    private LocalDateTime dataAutorizacao;
}
