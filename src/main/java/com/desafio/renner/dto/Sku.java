package com.desafio.renner.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Sku {
    @NotNull(message = "{desafio.error.field.null}")
    private Long sku;
    @NotNull(message = "{desafio.error.field.null}")
    private Long valor;
    @NotNull(message = "{desafio.error.field.null}")
    private Long quantidade;
}
