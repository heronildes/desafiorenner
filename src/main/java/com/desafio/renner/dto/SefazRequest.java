package com.desafio.renner.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class SefazRequest {

    private Long orderNumber;
    private String externalOrderNumber;
    private Customer customer;
    private List<Product> products;

}
