package com.desafio.renner.enums;

public enum TipoDocumento {

    CPF("CPF"),
    RG ("RG"),
    CTPS("CTPS"),
    CNH("CNH"),
    CNPJ("CNPJ");

    private final String documento;
    TipoDocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.documento);
        return sb.toString();
    }
}
