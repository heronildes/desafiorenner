package com.desafio.renner.service;

import com.desafio.renner.dto.CanalRequest;
import com.desafio.renner.dto.Cliente;
import com.desafio.renner.dto.Customer;
import com.desafio.renner.dto.NfeResponse;
import com.desafio.renner.dto.Product;
import com.desafio.renner.dto.SefazRequest;
import com.desafio.renner.dto.Tributo;
import com.desafio.renner.dto.VendaRequest;
import com.desafio.renner.dto.VendaResponse;
import com.desafio.renner.entities.Venda;
import com.desafio.renner.enums.VendaStatus;
import com.desafio.renner.feign.CanalClientGateway;
import com.desafio.renner.feign.SefazClientGateway;
import com.desafio.renner.feign.TributoClientGateway;
import com.desafio.renner.repositories.VendaRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.nonNull;

@Service
@Slf4j
public class AutorizarVendaService {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private Queue queue;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private VendaRepository repository;

    @Autowired
    private TributoClientGateway tributoClientGateway;
    @Autowired
    private SefazClientGateway sefazClientGateway;
    @Autowired
    private CanalClientGateway canalClientGateway;


    public VendaResponse autorizarVenda(VendaRequest vendaRequest) throws JsonProcessingException {

       vendaRequest.setDataRequisicao(LocalDateTime.now());
       sendQueueMessage(vendaRequest);
       return VendaResponse.builder()
               .status(VendaStatus.EM_PROCESSAMENTO)
               .dataResposta(vendaRequest.getDataRequisicao())
               .build();

    }


    public void processarAutorizacao(VendaRequest request) {

        SefazRequest sefazRequest = getSefazRequest(request);

        NfeResponse nfeResponse = (NfeResponse) sefazClientGateway.authorize(sefazRequest).getBody();
        CanalRequest canalRequest = CanalRequest.builder()
                .pdf(nfeResponse.getInvoice())
                .chaveNFE(nfeResponse.getNfeKey())
                .numeroPedido(sefazRequest.getOrderNumber())
                .numeroOrdemExterno(sefazRequest.getExternalOrderNumber())
                .dataEmissao(nfeResponse.getIssuanceDate())
                .status(VendaStatus.PROCESSADO.toString())
                .build();

        String mensagem = canalClientGateway.retornoVenda(canalRequest).getBody().toString();

        salvarVenda(request, sefazRequest, nfeResponse, canalRequest);
        log.info(mensagem);
    }

    private void salvarVenda(VendaRequest request,
                             SefazRequest sefazRequest,
                             NfeResponse nfeResponse,
                             CanalRequest canalRequest) {
        Venda venda = Venda.builder()
                .canal(request.getCanal())
                .chaveNfe(canalRequest.getChaveNFE())
                .codigoEmpresa(Long.parseLong(request.getEmpresa()))
                .codigoLoja(Long.parseLong(request.getLoja()))
                .numeroOrdemExterno(canalRequest.getNumeroOrdemExterno())
                .numeroPedido(canalRequest.getNumeroPedido().toString())
                .numeroPdv(request.getPdv())
                .pdf(canalRequest.getPdf())
                .situacao(canalRequest.getStatus())
                .valorTotal(getValorTotal(sefazRequest.getProducts()))
                .quantidade(getAmount(sefazRequest.getProducts()))
                .dataEmissao(canalRequest.getDataEmissao())
                .dataRequisicao(request.getDataRequisicao())
                .dataAtualizacao(LocalDateTime.now())
                .numeroConta(nfeResponse.getInvoiceNumber())
                .pdf(nfeResponse.getInvoice())
                .vendaRequest(nfeResponse.getInvoice())
                .build();

        repository.save(venda);
    }

    private SefazRequest getSefazRequest(VendaRequest request) {
        SefazRequest sefazRequest = SefazRequest.builder()
                .orderNumber(request.getOrdemPedido().getNumeroPedido())
                .externalOrderNumber(request.getOrdemPedido().getNumeroOrdemExterno())
                .customer(parseCustomer(request.getCliente()))
                .products(new ArrayList<>())
                .build();
        request.getItens().stream().forEach(sku -> {
            Tributo tributo = tributoClientGateway.getParceiros(sku.getSku());
            if(nonNull(tributo)) {
                Product product = Product.builder()
                        .sku(sku.getSku())
                        .amount(sku.getQuantidade())
                        .value(sku.getValor().doubleValue()/100)
                        .difaulValue(tributo.getValorDifaul())
                        .fcpIcmsValue(tributo.getValorFcpIcms())
                        .icmsValue(tributo.getValorIcms())
                        .pisValue(tributo.getValorPis())
                        .build();
                sefazRequest.getProducts().add(product);
            }
        });
        return sefazRequest;
    }


    private Double getValorTotal(List<Product> productList){
        AtomicReference<Double> valorTotal = new AtomicReference<>(0.00);
        productList.stream().forEach(product -> valorTotal.set(valorTotal.get() + product.getValue()));
        return valorTotal.get();
    }

    private Long getAmount(List<Product> productList){
        AtomicReference<Long> quantidadeTotal = new AtomicReference<>(0L);
        productList.stream().forEach(product -> quantidadeTotal.set(quantidadeTotal.get() + product.getAmount()));
        return quantidadeTotal.get();
    }

    private void sendQueueMessage(VendaRequest request) throws JsonProcessingException {
        String payload = objectMapper.writeValueAsString(request);
        rabbitTemplate.convertAndSend(queue.getName(), payload);
        log.info("send queue autorizar-venda-queue ");
    }

    private Customer parseCustomer(Cliente cliente){
        return Customer.builder()
                .id(cliente.getId())
                .name(cliente.getNome())
                .document(cliente.getDocumento())
                .documentType(cliente.getTipoDocumento().toString())
                .personType(String.valueOf(cliente.getTipoPessoa()))
                .address(cliente.getEndereco())
                .addressNumber(cliente.getNumeroEndereco())
                .addressComplement(cliente.getComplementoEndereco())
                .district(cliente.getBairro())
                .city(cliente.getCidade())
                .state(cliente.getEstado())
                .country(cliente.getCidade())
                .zipCode(cliente.getCep())
                .ibgeCode(cliente.getCodigoIbge())
                .phoneNumber(cliente.getTelefone())
                .email(cliente.getEmail())
                .build();
    }




}
