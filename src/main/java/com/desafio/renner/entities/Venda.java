package com.desafio.renner.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "VENDA")
public class Venda {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "VENDA_SEQ")
    @SequenceGenerator(name = "VENDA_SEQ", sequenceName = "VENDA_SEQ",  allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CANAL", nullable = false, length = 100)
    private String canal;

    @Column(name = "CODIGO_EMPRESA", nullable = false)
    private Long codigoEmpresa;

    @Column(name = "CODIGO_LOJA", nullable = false)
    private Long codigoLoja;

    @Column(name = "NUMERO_PDV", nullable = false)
    private Long numeroPdv;

    @Column(name = "NUMERO_PEDIDO", nullable = false, length = 38)
    private String numeroPedido;

    @Column(name = "NUMERO_ORDEM_EXTERNO", nullable = false, length = 38)
    private String numeroOrdemExterno;

    @Column(name = "VALOR_TOTAL", nullable = false)
    private Double valorTotal;

    @Column(name = "QTD_ITEM", nullable = false)
    private Long quantidade;

    @Column(name="VENDA_REQUEST", columnDefinition="CLOB NOT NULL")
    @Lob
    private String vendaRequest;

    @Column(name = "DATA_ATUALIZACAO", nullable = false)
    private LocalDateTime dataAtualizacao;

    @Column(name = "DATA_REQUISICAO", nullable = false)
    private LocalDateTime dataRequisicao;

    @Column(name = "CHAVE_NFE", length = 44)
    private String chaveNfe;

    @Column(name = "NUMERO_NOTA")
    private Long numeroConta;

    @Column(name = "DATA_EMISSAO")
    private LocalDateTime dataEmissao;

    @Column(name="PDF", columnDefinition="CLOB")
    @Lob
    private String pdf;

    @Column(name = "SITUACAO", nullable = false, length = 100)
    private String situacao;

    @Column(name = "MOTIVO", length = 255)
    private String motivo;




}
