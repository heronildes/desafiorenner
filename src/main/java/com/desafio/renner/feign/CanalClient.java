package com.desafio.renner.feign;

import com.desafio.renner.config.FeignConfiguration;
import com.desafio.renner.dto.CanalRequest;
import com.desafio.renner.dto.SefazRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "canalApi", url = "http://localhost:9292",
        configuration = FeignConfiguration.class)
public interface CanalClient {

    @PostMapping(value = "/callback-venda", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity retornoVenda(@RequestBody CanalRequest canalRequest);
}
