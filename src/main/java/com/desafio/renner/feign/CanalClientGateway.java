package com.desafio.renner.feign;

import com.desafio.renner.dto.CanalRequest;
import com.desafio.renner.dto.NfeResponse;
import com.desafio.renner.dto.SefazRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CanalClientGateway implements CanalClient{

    private CanalClient client;

    @Override
    public ResponseEntity retornoVenda(CanalRequest canalRequest) {
       // ResponseEntity<NfeResponse> responseEntity = client.retornoVenda(canalRequest);
        return ResponseEntity.of(Optional.of("Venda "+  canalRequest.getNumeroOrdemExterno() + "recebida com sucesso"));
    }


}
