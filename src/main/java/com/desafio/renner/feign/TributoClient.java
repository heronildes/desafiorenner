package com.desafio.renner.feign;

import com.desafio.renner.config.FeignConfiguration;
import com.desafio.renner.dto.Tributo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "tributarioApi", url = "http://localhost:9090",
        configuration = FeignConfiguration.class)
public interface TributoClient {

    @GetMapping(value = "/tributo", consumes = MediaType.APPLICATION_JSON_VALUE)
    Tributo getParceiros(@RequestParam(name = "sku") Long sku);
}
