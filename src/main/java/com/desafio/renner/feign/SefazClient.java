package com.desafio.renner.feign;

import com.desafio.renner.config.FeignConfiguration;
import com.desafio.renner.dto.SefazRequest;
import com.desafio.renner.dto.Tributo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "sefazApi", url = "http://localhost:9191",
        configuration = FeignConfiguration.class)
public interface SefazClient {

    @PostMapping(value = "/authorize", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity authorize(@RequestBody SefazRequest sefazRequest);
}
