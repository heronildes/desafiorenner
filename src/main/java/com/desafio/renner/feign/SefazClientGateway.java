package com.desafio.renner.feign;

import com.desafio.renner.dto.NfeResponse;
import com.desafio.renner.dto.SefazRequest;
import com.desafio.renner.dto.Tributo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SefazClientGateway implements SefazClient{

    private SefazClient client;

    @Override
    public ResponseEntity authorize(SefazRequest sefazRequest) {
       // ResponseEntity<NfeResponse> responseEntity = client.authorize(sefazRequest);
        NfeResponse nfeResponse = NfeResponse.builder()
                .nfeKey("43210392754738001134550040000159551330237069")
                .invoice("NDMyMTAzOTI3NTQ3MzgwMDExMzQ1NTAwNDAwMDAxNTk1NTEzMzAyMzcwNjk")
                .invoiceNumber(Long.parseLong("0237069"))
                .issuanceDate(LocalDateTime.of(2022,11,11,15, 38,0,012))
                .build();
        return ResponseEntity.of(Optional.of(nfeResponse));
    }


}
