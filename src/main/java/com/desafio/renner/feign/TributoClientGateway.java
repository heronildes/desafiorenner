package com.desafio.renner.feign;

import com.desafio.renner.dto.Tributo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class TributoClientGateway implements TributoClient{

    private TributoClient client;


    @Override
    public Tributo getParceiros(Long sku) {
       // return client.getParceiros(sku);
        return obterParceiroMock(sku);
    }

    private Tributo obterParceiroMock(Long sku){
        List<Tributo> tributoList = List.of(Tributo.builder()
                        .sku(324226428L)
                        .valorIcms(38L)
                        .valorPis(12L)
                        .valorDifaul(9L)
                        .valorFcpIcms(58L)
                        .build(),
                Tributo.builder()
                        .sku(286441499L)
                        .valorIcms(38L)
                        .valorPis(12L)
                        .valorDifaul(9L)
                        .valorFcpIcms(58L)
                        .build(),
                Tributo.builder()
                        .sku(183675297L)
                        .valorIcms(38L)
                        .valorPis(12L)
                        .valorDifaul(9L)
                        .valorFcpIcms(58L)
                        .build() );

        return tributoList.stream()
                .filter(tributo -> tributo.getSku().equals(sku))
                .findAny().get();

    }
}
