package com.desafio.renner.controller;

import com.desafio.renner.dto.VendaRequest;
import com.desafio.renner.dto.VendaResponse;
import com.desafio.renner.service.AutorizarVendaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/desafio")
public class VendaController {

    @Autowired
    private AutorizarVendaService service;

    @PostMapping("/autorizar-venda")
    public ResponseEntity autorizarVenda(@Valid @RequestBody VendaRequest request)
            throws Exception {
        VendaResponse response = service.autorizarVenda(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
